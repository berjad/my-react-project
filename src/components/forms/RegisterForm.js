import React, { useState } from "react";
import { registerUser } from '../../api/user.api';

const RegisterForm = (props) => {

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [registerError, setRegisterError] = useState('')

  const onRegisterClicked = async (ev) => {

    setIsLoading(true)

        try {
            const result = await registerUser(username, password);
            console.log(result);
            
        } catch (e) {
            setRegisterError(e.message || e);
        } finally {
            setIsLoading(false)

        }
  };

  const onUsernameChanged = (ev) => setUsername(ev.target.value.trim());
  const onPasswordChanged = (ev) => setPassword(ev.target.value.trim());



  return (
    <div className="container">
    <form>
      <div className="form-group">
        <label>Username: </label>
        <input
        className="form-control"
          type="text"
          placeholder="Enter a username"
          onChange={onUsernameChanged}
        />
      </div>

      <div className="form-group">
        <label>Password: </label>
        <input
        className="form-control"
          type="password"
          placeholder="Enter a password"
          onChange={onPasswordChanged}
        />
      </div>

      <div>
        <button type="button" className="btn btn-primary" onClick={onRegisterClicked}>
          Register
        </button>
      </div >

      { isLoading && <div className="alert alert-primary" role="alert">Registering user....</div> }

  { registerError && <div className="alert alert-danger" role="alert">{registerError}</div> }
    </form>
    </div>
  );
};

export default RegisterForm;
