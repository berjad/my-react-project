import React from "react";

const LoginForm = (props) => {
  const onLoginClicked = (event) => {
    console.log("Event: ", event.target);

    props.click({
      success: true,
      message: "Login was a success",
    });
  };
  return (
    <div className="container">
      <form>
        <div className="form-group">
          <label>Username: </label>
          <input
            className="form-control"
            type="text"
            name="username"
            id="username"
            placeholder="Enter your username"
          />
        </div>

        <div className="form-group">
          <label>Password: </label>
          <input
            className="form-control"
            type="password"
            name="password"
            id="password"
            placeholder="Enter your password"
          />
        </div>

        <div>
          <button type="button" className="btn btn-primary" onClick={onLoginClicked}>
            Login
          </button>
        </div>
      </form>
    </div>
  );
};

export default LoginForm;
