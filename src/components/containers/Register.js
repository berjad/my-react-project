import React from "react";
import RegisterForm from "../forms/RegisterForm";

const Register = () => {

  const handleRegisterClicked = (result) => {
    console.log('Triggered from registerForm', result);
    
  }

  return (
    <div className="container">
    <h2>Register for Survey Puppy</h2>

    <RegisterForm click={ handleRegisterClicked } />
  </div>
  )

  };

export default Register;
